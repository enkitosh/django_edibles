django-edibles
==============

Editable tables in django with filtering

Warning: This library is still on development stage.

Source:

Usage:
------

    Note: This library relies on django_tables2 so make sure
          you have that installed as well
          
    Source:
        http://django-tables2.readthedocs.org/en/latest/

    1. Add django_edibles to your settings
    
        INSTALLED_APPS = (
            'django_edibles',
        )

    2. Import the decorator wherever you setup your tables
        
        Example: tables.py

        import django_tables2 as tables
        from django_edibles.decorators import editable
        from .models import TestClass

        @editable(search=True) -- search is optional
        class TestTable(tables.Table):
            name = tables.Column()
            age  = tables.Column()

            class Meta:
            attrs = { "class" : "table",      --Optional
                      "id"    : "TestTable" } --Required
            model = TestClass
            fields = ('name', 'age')

    3. Make sure you include the javascript files in static/js:
        edibles.js -- handles tables
        utils.js   -- csrf utilities etc...

    4. In your template
        
        <div class="edibles">
            <form action="/edibles/" id="edible-form" method="post">
                {% csrf_token %}
                {% render_table table %}
            </form>
        </div>

    5. Handle response with runRequest in models.py
       
        class SomeGreatView(View):
            def get(self, request):
                pass

            def post(self, request):
                if request.is_ajax():
                    response = runRequest(request, 
                                          TestClass, -- model class
                                          TestTable) -- table class
                return HttpResponse(response, content_type="application/json")

        

        
