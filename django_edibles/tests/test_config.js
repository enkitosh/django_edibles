require.config({
    baseUrl : '',
    paths : {
        //jquery
        "jquery" : "../static/js/jquery-1.11.0.min",
        // Test | Assertion libraries
        "chai"   : "node_modules/chai/chai",
        "mocha"  : "node_modules/mocha/mocha",
        // Script modules
        "edibles": "../static/js/edibles",
        "search" : "../static/js/search",
        "utils"  : "../static/js/utils",
        // Test files
        "edibles_tests" : "edibles_tests"
    },

    shim : {
        "mocha" : {
            init  : function() {
                this.mocha.setup('bdd');
                return this.mocha;
            }
        }
    }
});

require(["chai", "mocha"], function(chai, mocha) {

    require(["edibles_tests"], function(App) {
        mocha.run();
    });
});

