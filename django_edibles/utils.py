import base64

def obfuscate(id_str):
    return base64.b64encode(bytes(id_str))

def clarify(id_str):
    return base64.b64decode(bytes(id_str))
