import django_tables2 as tables
from django_edibles.utils import obfuscate

def stdattrs():
    """ Standard attributes for hidden columns """
    return {'td':{'class':'hidden'},\
            'th':{'class':'hidden'}}

def editable(**args):
    def edit_table(f):
        # Add hidden edible column
        f.edible_id = tables.Column(attrs=stdattrs())
        f.base_columns['edible'] = f.edible_id
        incl_fields = ('edible',)


        if args.get('editable'):
            # Add hidden edible column
            f.editable  = tables.Column(attrs=stdattrs())
            f.base_columns['editable'] = f.editable
            incl_fields = incl_fields +  ('editable',)

        # Add search column if defined
        if args.get('search'):
            f.edible_search = tables.Column(attrs=stdattrs())
            f.base_columns['edible_search'] = f.edible_search
            incl_fields = incl_fields + ('edible_search',)

        if args.get('remove'):
            # Add removal column
            removal = args.get('remove')
            f.edible_remover = tables.TemplateColumn(verbose_name  = removal,\
                                                     template_name = "django_edibles/templateColumns/remove.html")
            f.base_columns['edible_remover'] = f.edible_remover
            incl_fields = incl_fields + ('edible_remover',)

        if f._meta.fields:
            f._meta.fields = tuple(f._meta.fields) + incl_fields
            if f._meta.sequence:
                f._meta.sequence = f._meta.sequence + list(incl_fields)
        def wrapper(*args, **kwargs):
            # Snatch pk
            data = [a for a in args[0]]
            for d in data:
                if f._meta and f._meta.model:
                    # Obfuscate primary key
                    d.__dict__['edible'] = obfuscate(str(d.id).encode()).decode('utf-8')
                else:
                    # Meta model not being used, check for id
                    if(d['id']):
                        d['edible'] = obfuscate(str(d['id']).encode())
            args = (data,)
            return f(*args, **kwargs)
        return wrapper
    return edit_table

