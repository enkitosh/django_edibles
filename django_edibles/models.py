from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django_edibles.utils import clarify
import json

# Create your models here.

class TestClass(models.Model):
    name = models.CharField(max_length=250)
    age  = models.CharField(max_length=3)

def runCustomRequest(request):
    try:
        data = request.body.decode('utf-8')
        json_data = json.loads(data)

        if json_data.get('filter'):
            pass
        else:
            json_data['id_row'] = clarify(json_data['id_row'].encode()).decode('utf-8')

        return json_data
    except Exception as e:
        print(e)

def runRequest(request, model, table):
    data = request.body.decode('utf-8')
    json_data = json.loads(data)
    response = ''
    if "filter" in json_data:
        query = { json_data['filter'].lower(): \
                  json_data['query'] }
        data = model.objects.filter(**query)

        if data:
            filterTable = table(data)
        else:
            filterTable = table(model.objects.all())
                        
        response  = json.dumps({'html' : filterTable.as_html()})
    else:
        pk_clar = clarify(json_data['id_row'].encode())
        if json_data['action'] == "drop":
            model.objects.get(pk=pk_clar).delete()
            response = json.dumps({'status' : 'ok',\
                                    'id'    : 'delete' })
        elif json_data['action'] == "update":
            obj    = model.objects.get(pk=pk_clar)
            before = getattr(obj, json_data['class'])
            setattr(obj, json_data['class'], json_data['value'])
            obj.save()
            response = json.dumps({'status' : 'ok',\
                                   'id'     : 'update',\
                                   'before' : before,
                                   'after'  : json_data['value']})
    return response

