define(["jquery","utils"], function($, Utils) {
    "use strict";

/**************************************************************/
var Slices = function () {
/**
 *  - buffer   : to store edited table cells
 *  - response : to store reponse from sent buffer data
 *  - sliceUrl : Location to send slices to
 */
    var buffer = {};
    var response;
    var sliceUrl;

    function setUrl(url) {
        sliceUrl = url;
    }

    function getResponse() {
        return response;
    }

    function setResponse(res) {
        response = res;
    }

    /**
     * @cell   - the current cell being sliced
     * @action - action being run (create | update | drop)
     * @value  - custom value passed if table cell is complicated (select etc..)
     */
    function makeSlice(cell, action, value) {
        // Construct a slice from a cell
        // add slice to buffer
        var slice = {
            // ID of the table being edited
            'table'  : $(cell).closest('table').attr('id'),
            // Class name of cell being edited
            'class'  : $(cell).attr('class'),
            // Action - create | update | drop
            'action' : action,
            // Value, if being updated
            'value'  : ($(cell).val()) ? $(cell).val() : value,
            // Id of the row being updated
            'id_row' : $(cell).closest('tr').children('.edible').text()
        };

        buffer = slice;
        return buffer;
    }

    function sendBuffer() {

        Utils.beforeSend();
        var self = this;

        console.log("Sending buffer: " + JSON.stringify(buffer));

        $.ajax({
            async:false,
            url : sliceUrl,
            type: 'POST',
            data: JSON.stringify(buffer),
            contentType: 'application/json',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                response = data;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                    console.log("Edibles.js error: " + errorThrown);
            }
        });

        return response;

    }

    return {
        setUrl     : setUrl,
        makeSlice  : makeSlice,
        sendBuffer : sendBuffer,
        getResponse: getResponse
    };

};
/***************************************************************/
//Table utilities
var TableUtils = function() {

    var slice = new Slices();
    slice.setUrl($('edible-form').attr('action'));

    //Functions
    function editCell() {
        var cell = this;
        var input = $('<input/>').attr('value',$(this).text());
        var w = $(this).innerWidth();
        var h = $(this).innerHeight();
        input.css('display', 'block');
        $(this).empty();
        $(this).append(input);
        input.focusout(function() {
            $(this).parent().text($(this).val());
            slice.makeSlice(cell,"update", $(this).val());
            slice.sendBuffer();
        });
        input.focus();
    }

    function updateCell () {
        // Called after a column is edited

        var colSelect = $(this).find('select');
        var select;
        if($.isEmptyObject(colSelect)) {
            // normal td, or at least not select
        } else {
            select = $(":selected",colSelect).val();
            console.log(select);
        }

        slice.makeSlice(this,"update", select);
        slice.sendBuffer();
    }

    function addRow () {
    // Add a row to a table
        // fetch id
        var table   = this.id.replace("-plus", "");
        // Clone last row in table
        var clone   = $("#"+ table + " tr:last").clone()
        
        var input = "<input type='text' class='form-control'\
                     style='display:table-cell; width:100%' />";

        clone.find("td").each(function() {
            // clear text
            $(this).text("");
            // Add input boxes on each td for user edits
            $(this).html(input);
        });

        // We follow django_tables2 structure of naming tr classes
        var classID = (clone.closest('tr').attr("class") === "even") ? "odd" : "even";

        // Construct html for row to append to table
        var tr_html_ = "<tr class='"+classID+"'>"+clone.html()+"</tr>";

        //Add new row to table
        $("#"+table+" tr:last").after(tr_html_);
    }
    
    function dropRow() {
        // Called when user deletes a row

        slice.makeSlice(this,"drop");
        var response = slice.sendBuffer();

        if(response['status'] === "ok") {
            $(this).closest("tr").fadeOut("normal", function() {
                $(this).closest("tr").remove();
            });
        }
    }


    /**
     * @id - Table id
     */
    function translateFields(id) {
        // Used to fetch real class names of td elements
        // Returns a translation table {'header' : 'classname'}
        var translated = {};
        $("#"+id + " th").each(function () {
            var classname = $(this).attr('class');
            var header    = $(this).children(':first').text();

            if (classname.match("^edible")) {
                // skip
            } else {
                translated[header] = classname.substr(0,classname.indexOf(' '));
            }
        });

        return translated;
    }

    return {
        addRow          : addRow,
        dropRow         : dropRow,
        editCell        : editCell,
        translateFields : translateFields
    };

};

/***************************************************************/
// UI-components
//
var UI = function() {
      
    var tUtils = new TableUtils();

    function extra_table_ui(id) {
        /*Add a + icon for adding new rows to table*/
        /*
        $("#" + id).append(
            "<span id='"+config.getTableId() + 
            "-plus' class='glyphicon glyphicon-plus'></span>");
        */
    }

    function delegates(id) {

        // Add row
        $(document).on("click", "#" + id + "-plus", tUtils.addRow);

        // Edit row
        $(document).on("click", "td", tUtils.editCell);

        /*General settings for when a td input element is changed*/
        $(document).on("change", "td", tUtils.updateCell);

        /*Remove row*/
        $(document).on("click", ".edible_remover", tUtils.dropRow);

    }

    function setup(id) {
        extra_table_ui(id);
        delegates(id);
    }
    
    return {
        setup : setup,
    };

};


var Search  = (function() {
    /*  Config:
     *      attach : Search box will be prepended before this div basically
     *      type   : "aggressive", "standard", "lazy"
     *      fields : fields to search in, you can exclude fields here
     *      attrs  : {class:'form-control',id='crazySearch'}
     *      filter : Turn on filters when searching
     *      url    : url for POST
     * */
    var config = { 
        // ID of element we are searching in
        'attach' : '', 
        // Fields: [{'filter-name' : 'db-field}]
        'fields' : {}, 
        // url for POST-ing search values
        'url'    : '', 
        // Search options
        'search'  : { 
            'id' : '', 
            // Search type: ['standard', 'aggressive', 'lazy']
            'type'   : 'standard',
            // default placeholder value
            'placeholder' : 'Search',
        },  
        // Filter options
        'filter' : { 
            'id'          : '', 
            'on'          : false,
            'placeholder' : 'Filter'
        }   
    };  

    function setupUI() {
            
        // Search_box id is: Attached table id + -search
        config.search.id = config.attach + "-search";
        config.filter.id = config.attach + "-filter";

        // TODO: consider some templating for UI
        var ui_html = '<div class="input-group">';
            ui_html+= '<div class="input-group-btn">';
            ui_html+= '<button type="button"';
            ui_html+= ' class="btn btn-default dropdown-toggle"';
            ui_html+= ' data-toggle="dropdown">'+config.filter.placeholder + ' ';
            ui_html+= '<span class="caret"></span></button>';
            ui_html+= '<ul id="'+config.filter.id+'" class="dropdown-menu">';
            ui_html+= '</ul></div><input id='+ config.search.id+' type="text"';
            ui_html+= 'class="form-control" /></div>';

        var wraps = $("<div />")
                    .attr({'class' : 'search_bar'});

        $(ui_html).appendTo(wraps);
        $(wraps).insertBefore('#'+config.attach);

        $.each(config.fields, function( key, value) {
            $("<li class='"+value+"'><a href='#'>"+key+"</a></li>").appendTo("#"+config.filter.id);
        });

    }

    function search(str) {

        if(config.search['type'] === "aggressive") {
            // Aggresive sends search query on each input letter
                send(str);
        } else if(config.search['type'] === "standard") {
            // Standard waits for 4 characters or more
            if(str.length >= 4) {
                send(str);
            }
        } else if(config.search['type'] === "lazy") {
            // lazy waits for a whitespace
            if(hasWhiteSpace(str)) {
                send(str);
            }
        }
    }

    function send(str) {

        var key = $('#'+ config.filter.id).parent().find('.btn-default').html();

        Utils.beforeSend();

        $.ajax({
            url: config.url,
            type: "POST",
            data: JSON.stringify(
                {'id'     : config.attach,
                 'query'  : str,
                 'filter' : config.fields[key] }
            ),
            success: function(data) {
                $('#'+config.attach).html(data['html']);
                console.log(data);
              }
        });
    }

    /*Helpers*/
    function hasWhiteSpace(s) {
          return /\s/g.test(s);
    }

    /*main*/
    function initialize() {
        setupUI();
        /*Initialize with config*/
        $('#'+config.search.id).on('input', function(e) {
            e.preventDefault();
            search($(this).val())
        });

        $("#" + config.filter.id + " li a").click(function(){
        $(".btn:first-child").text($(this).text());
        $(".btn:first-child").val($(this).text());
        });
    }

    return {
        initialize: initialize,
        config: config
    };

});

var Edibles = (function() {

    function main() {
        var isEdible = false;
        var hasSearch = false;
        var counter = 0;
        
        $("table").each(function() {


            $('#'+this.id  + ' th').each(function() {
                // TODO: we only need the first column returining edible or search
                //       not all of them
                var str = $(this).text();
                if(str === "Edible") {
                    isEdible = true;
                }
                if(str === "Edible Search")
                {
                        hasSearch = true;
                }
            });

            if(isEdible) {
                var TUtils = new TableUtils();
                var UI_ = new UI();
                UI_.setup(this.id);
            }
            
            if(hasSearch) {
                var TUtils = new TableUtils();
                var SearchBox = new Search();
                SearchBox.config.attach = this.id
                SearchBox.config.url = $("#edible-form").attr('action');
                SearchBox.config.fields = TUtils.translateFields(this.id);
                SearchBox.initialize();
            }
        });
    }

    return {
        main: main
    };

})();

    return {
        Edibles    : Edibles,
        Slices     : Slices,
        TableUtils : TableUtils,
        UI         : UI,
        Search     : Search
    };

});
