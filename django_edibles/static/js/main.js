require.config({
    baseUrl: '/static/js/',
    paths  : {
        'jquery'    : 'jquery-1.11.0.min',
        'bootstrap' : 'bootstrap.min',
        'edibles'   : 'edibles',
        'search'    : 'search',
        'utils'     : 'utils'
    },
    shim : {
        "bootstrap" : {
            deps : ["jquery"]
        }       
    },
    urlArgs: "ts="+new Date().getTime()
});


require(["jquery", "bootstrap"], function($, Bootstrap) {
    console.log($);
});

require(["edibles"], function(App) {
    App.Edibles.main();
});

