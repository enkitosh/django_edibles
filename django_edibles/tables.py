# -*- coding: utf-8 -*-

import django_tables2 as tables
from django_edibles.decorators import editable
from .models import TestClass

@editable(search=True, remove="Eyða")
class TestTable(tables.Table):
    name = tables.Column(verbose_name="nafn")
    age  = tables.Column(verbose_name="aldur")

    class Meta:
        attrs = { "class" : "table",
                  "id"    : "TestTable" }
        model = TestClass
        fields = ('name', 'age')
