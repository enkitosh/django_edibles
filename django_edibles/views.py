from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render
from django_edibles.tables import TestTable
from .models import TestClass, runRequest

class MyTable(View):
    def get(self, request):

        data = TestClass.objects.all()
        table = TestTable(data)
        return render(request, 'django_edibles/tables.html',\
                      {'table' : table})
    def post(self, request):
        if request.is_ajax():
            response = runRequest(request, TestClass, TestTable)
            print(response)
        return HttpResponse(response, content_type="application/json")


