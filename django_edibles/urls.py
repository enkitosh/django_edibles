from django.conf.urls import patterns, include, url
from django_edibles.views import MyTable

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dj_edibles.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^edibles/$',MyTable.as_view()),
    url(r'^admin/', include(admin.site.urls)),
)
